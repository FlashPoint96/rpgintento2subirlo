﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;



public class ScriptPJ : MonoBehaviour
{
    private AsyncOperation sceneAsync;

    public GameEvent[] Events;
    public Camera cam;
    private Rigidbody2D RB;
    public Animator animator;
    private String lastKey;

    public bool stealth = false;
    private static bool EnOverWorld = true;
    public PjSettings settings;

    public float health;
    public float hpMax;
    public float damage;
    private float velocidad;
    public int stamina;
    public int xp;
    public int level;
    public Vector3 position;


    // Start is called before the first frame update
    void Start()
    {
        velocidad = settings.Velocity;
        cam = this.GetComponentInChildren<Camera>();
        RB = this.GetComponent<Rigidbody2D>();
        cam.gameObject.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, cam.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        //this.settings.Position = this.transform.position;
        //Moverse();
        Moverse2();
        //cam.gameObject.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, cam.transform.position.z);
        //GetKeyDown(KeyCode.R)
        if (Input.GetMouseButton(0))
        {
            StartCoroutine(Pegar());
            //PrintPosition();
        }
        if (Input.GetKey(KeyCode.F))
        {
            this.settings.Health--;
        }
        Stealth();
    }
    public void SaveScrObject()
    {
        settings.Health = this.health;
        settings.HPmax = this.hpMax;
        settings.Damage = this.damage;
        settings.Velocity = this.velocidad;
        settings.Stamina = this.stamina;
        settings.XP = this.xp;
        settings.Level = this.level;
        settings.Position = this.transform.position;
    }

    public void LoadScrObject()
    {
        this.health = settings.Health;
        this.hpMax = settings.HPmax;
        this.damage = settings.Damage;
        this.velocidad = settings.Velocity;
        this.stamina = settings.Stamina;
        this.xp = settings.XP;
        this.level = settings.Level;
        this.transform.position = settings.Position;
    }

    private void Stealth()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            velocidad = 2f;
            stealth = true;
        }
        else
        {
            velocidad = settings.Velocity;
            stealth = false;
        }

    }

    private void Moverse2()
    {
        if (Input.GetKey(KeyCode.W))
        {
            if (Input.GetKey(KeyCode.D))
            {
                lastKey = "d";
                animator.SetTrigger("WalkD");
                RB.velocity = new Vector2(velocidad/2, velocidad/2);
            }else if (Input.GetKey(KeyCode.A))
            {
                lastKey = "a";
                animator.SetTrigger("WalkA");
                RB.velocity = new Vector2(-velocidad / 2, velocidad / 2);
            }
            else
            {
                lastKey = "w";
                animator.SetTrigger("WalkW");
                RB.velocity = new Vector2(0, velocidad);
            }
        }else if (Input.GetKey(KeyCode.S))
        {
            if (Input.GetKey(KeyCode.D))
            {
                lastKey = "d";
                animator.SetTrigger("WalkD");
                RB.velocity = new Vector2(velocidad/2, velocidad * -1/2);
            }
            else if (Input.GetKey(KeyCode.A))
            {
                RB.velocity = new Vector2(-velocidad / 2, velocidad * -1/2);
                lastKey = "a";
                animator.SetTrigger("WalkA");
            }
            else
            {
                lastKey = "s";
                animator.SetTrigger("WalkS");
                RB.velocity = new Vector2(0, velocidad * -1);
            }
        }else if (Input.GetKey(KeyCode.D))
        {
            lastKey = "d";
            animator.SetTrigger("WalkD");
            RB.velocity = new Vector2(velocidad, RB.velocity.y);
        }else if (Input.GetKey(KeyCode.A))
        {
            lastKey = "a";
            animator.SetTrigger("WalkA");
            RB.velocity = new Vector2(-velocidad, RB.velocity.y);
        }
        else
        {
            RB.velocity = new Vector2(0, 0);
        }

    }

    //private void PrintPosition()
    //{
    //    Vector3 tilePosition;
    //    Vector3Int coordinate = new Vector3Int(0, 0, 0);
    //    for (int i = 0; i < Tilemap.size.x; i++)
    //    {
    //        for (int j = 0; j < Tilemap.size.y; j++)
    //        {
    //            coordinate.x = i; coordinate.y = j;
    //            tilePosition = Tilemap.CellToWorld(coordinate);
    //            print(string.Format("Position of tile [{0}, {1}] = ({2}, {3})", coordinate.x, coordinate.y, tilePosition.x, tilePosition.y));
    //        }
    //    }
    //}

    private void Moverse()
    {
        if (Input.GetKey("a"))
        {
            lastKey = "a";
            animator.SetTrigger("WalkA");
            RB.velocity = new Vector2(-velocidad, RB.velocity.y);
        }
        else if (Input.GetKey("d"))
        {
             lastKey = "d";
            animator.SetTrigger("WalkD");
            RB.velocity = new Vector2(velocidad, RB.velocity.y);
        }
        else if (Input.GetKey("w"))
        {
            lastKey = "w";
            animator.SetTrigger("WalkW");
            RB.velocity = new Vector2(0, velocidad);
        }
        else if (Input.GetKey("s"))
        {
            lastKey = "s";
            animator.SetTrigger("WalkS");
            RB.velocity = new Vector2(0, velocidad * -1);
        }
        else
        {
            RB.velocity = new Vector2(0, 0);
        }
    }

    IEnumerator Pegar()
    {

        if (lastKey == "a")
        {
            animator.SetTrigger("AttackA");
        }else if(lastKey == "d")
        {
            animator.SetTrigger("AttackD");
        }else if (lastKey == "s")
        {
            animator.SetTrigger("AttackS");

        }else if(lastKey == "w")
        {
            animator.SetTrigger("AttackW");
        }

        yield return 0;
      
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            this.settings.Health--;
            String buscar;
            if (this.settings.Health <= 0)
            {
                buscar = "PJDies";
            }
            else
            {
                buscar = "PJGetHit";
            }
            foreach(GameEvent Ge in Events)
            {
                if (Ge.name.Equals(buscar))
                {
                    Ge.Raise();
                }
            }
        }
        else
        {
            Vector3[] Forrestexit = { new Vector3(0, -5, 0), new Vector3(0, -4, 0), new Vector3(1, -5, 0), new Vector3(1, -4, 0), new Vector3(-1, -4), new Vector3(-1, -5) };
            Vector3 pjPosition = new Vector3(collision.GetContact(0).point.x, collision.GetContact(0).point.y);
            Vector3 doorCave = new Vector3(-18, 52, 0);
            Vector3 doorMountain = new Vector3(-59, 52, 0);
            Vector3 exitForest = new Vector3(-1, -1, 0);
            Vector3 exitCave = new Vector3(1, -5, 0);
            Vector3 exitMountain = new Vector3(0, -4, 0);
            collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition);
            print(collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition));

            if (collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition) == new Vector3(12, -9, 0) && SceneManager.GetActiveScene().buildIndex == 0)
            {
                this.settings.OverWorldPosition = this.transform.position;
                StartCoroutine(SceneSwitch(1,0,new Vector2(0,0)));
            }
            else if (collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition) == exitForest && SceneManager.GetActiveScene().buildIndex == 1)
            {
                StartCoroutine(SceneSwitchVuelta(0, 1));
            }
            else if (collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition) == doorMountain && SceneManager.GetActiveScene().buildIndex == 0)
            {
                this.settings.OverWorldPosition = this.transform.position;
                StartCoroutine(SceneSwitch(2, 0, new Vector2(0, 0)));
            }
            else if (collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition) == doorCave && SceneManager.GetActiveScene().buildIndex == 0)
            {
                this.settings.OverWorldPosition = this.transform.position;
                StartCoroutine(SceneSwitch(3, 0, new Vector2(0, 0)));
            }
            else if (collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition) == exitMountain && SceneManager.GetActiveScene().buildIndex == 2)
            {
                StartCoroutine(SceneSwitchVuelta(0,2));
            }
            else if (collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition) == exitCave && SceneManager.GetActiveScene().buildIndex == 3)
            {
                StartCoroutine(SceneSwitchVuelta(0, 3));
            }
        }
    }

    IEnumerator SceneSwitchVuelta(int v1, int v2)
    {
        AsyncOperation load = SceneManager.LoadSceneAsync(v1, LoadSceneMode.Additive);
        yield return load;
        SceneManager.MoveGameObjectToScene(this.gameObject, SceneManager.GetSceneByBuildIndex(v1));
        this.transform.position = this.settings.OverWorldPosition;
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(v1));
        SceneManager.UnloadSceneAsync(v2);
    }

    IEnumerator SceneSwitch(int v, int v1,Vector2 pos)
    {
        AsyncOperation load = SceneManager.LoadSceneAsync(v, LoadSceneMode.Additive);
        yield return load;
        SceneManager.MoveGameObjectToScene(this.gameObject, SceneManager.GetSceneByBuildIndex(v));
        this.transform.position = pos;
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(v));
        SceneManager.UnloadSceneAsync(v1);
    }


  
}
