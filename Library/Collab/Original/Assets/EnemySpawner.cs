﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public List<Vector2> respawns;
    // Start is called before the first frame updatew
    void Start()
    {
        RellenarLista();
        InvokeRepeating("InvocarEnemys", 0f, 15f);
    }
    public void RellenarLista()
    {
        respawns = new List<Vector2>();
        respawns.Add(new Vector2(23f, -15f));
        respawns.Add(new Vector2(-5f, -10f));
        respawns.Add(new Vector2(15f, 5f));
    }

    public void InvocarEnemys()
    {
        GameObject Enemy = PoolEnemys.Instance.GetEnemys();
        if(Enemy != null)
        {
            Enemy.transform.position = respawns[Random.Range(0, 2)];
            Enemy.SetActive(true);
        }
    }
}
