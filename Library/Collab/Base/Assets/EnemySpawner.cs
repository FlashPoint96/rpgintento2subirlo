﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("InvocarEnemys", 0f, 15f);
    }

    public void InvocarEnemys()
    {
        GameObject Enemy = PoolEnemys.Instance.GetEnemys();
        if(Enemy != null)
        {
            Enemy.transform.position = new Vector2(0, 0);
            Enemy.SetActive(true);
        }
    }
}
