﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthBarPJScript : MonoBehaviour
{
    public PjSettings J1;
    private float MaxVida;
    private float VidaActual;
    private void Start()
    {
        MaxVida = J1.HPmax;
    }
    public void ActualizarVida()
    {
        MaxVida = J1.HPmax;
        VidaActual = J1.Health;
        this.GetComponent<Image>().fillAmount = VidaActual / MaxVida;
    }
}
