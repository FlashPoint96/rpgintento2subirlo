﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "HealthBar", menuName = "ScriptableObjects/HealthBar", order = 1)]
public class HealthBar : ScriptableObject
{
    public float barDisplay;
    public Vector2 Pos;
    public Vector2 Size;
    public Texture2D emptyTex;
    public Texture2D fullTex;

}
