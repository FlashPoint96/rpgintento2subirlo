﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public PjSettings playerSettings;
    void Update()
    {
        Save();
        Loader();
    }
    private void Save()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            StartCoroutine(Saving());
            string jSon = JsonUtility.ToJson(playerSettings);
            File.WriteAllText("PlayerSettings.json", jSon);
            
            print("SAVED GAME");
        }
    }
    private void Loader()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            StartCoroutine(Loading());
            string jSon = File.ReadAllText("PlayerSettings.json");
            JsonUtility.FromJsonOverwrite(jSon, playerSettings);
            print("LOAD GAME");
        }
    }
    IEnumerator Saving()
    {
        this.GetComponent<ScriptPJ>().SaveScrObject();
        yield return new WaitForSeconds(1f);

    }
    IEnumerator Loading()
    {
        this.GetComponent<ScriptPJ>().LoadScrObject();
        yield return new WaitForSeconds(1f);
    }
}
