﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaPJ : MonoBehaviour
{
    public PjSettings J1;
    private float MaxStamina;
    private float StaminaActual;
    private void Start()
    {
        MaxStamina = J1.MaxStamina;
    }
    public void ActualizarStamina()
    {
        MaxStamina = J1.MaxStamina;
        StaminaActual = J1.Stamina;
        this.GetComponent<Image>().fillAmount = StaminaActual / MaxStamina;
    }
}
