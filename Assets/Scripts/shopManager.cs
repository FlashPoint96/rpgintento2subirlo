﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class shopManager : MonoBehaviour
{

    public Text[] texto = new Text[10];
    public List<Item> lista = new List<Item>();


    public PjSettings pj;

    public Text monedas;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        monedas.text = "Monedas: " + pj.monedas;

        for (int i = 0; i < lista.Count; i++)
        {
            if (lista[i] == null)
            {
                texto[i].text = "Empty";
            }
            else
            {


                if (lista[i].name == "Botas" || lista[i].name == "Espada" || lista[i].name == "Armadura" || lista[i].name == "Botas+" || lista[i].name == "Espada+" || lista[i].name == "Armadura+")
                {

                    texto[i].text = lista[i].name + " LvL " + lista[i].nivel + " Precio: " + lista[i].precio;

                }
                else
                {

                    texto[i].text = lista[i].name + " Precio: " + lista[i].precio;
                }

            }

        }
    }
}
