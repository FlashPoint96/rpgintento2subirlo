﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LvLUI : MonoBehaviour
{
    
    private TextMeshProUGUI texto;
    public PjSettings PJ;
    public static int lvl = 0;
    void Start()
    {
        texto = GetComponent<TextMeshProUGUI>();
    }

    public void actualizar()
    {
        texto.text = "LvL: " + PJ.Level.ToString();
    }

}
