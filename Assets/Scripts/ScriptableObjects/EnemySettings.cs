﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemySettings", menuName = "ScriptableObjects/EnemySettings", order = 1)]
public class EnemySettings : ScriptableObject
{
    public Vector3 Position;
    public float Health;
    public float MaxHealth;
    public float Damage;
    public Vector3 LocalScale;
    public float Speed;
    public Color color;
    public Vector3[] Ruta;
    public int Level;
    public int xpDrop;
    public float DamageRange;
    public float AtackCD;
}
