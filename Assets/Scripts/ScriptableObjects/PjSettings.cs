﻿using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(fileName = "PjSettings", menuName = "ScriptableObjects/PjSettings", order = 1)]
public class PjSettings : ScriptableObject
{
    public float Health;
    public float HPmax;
    public float MaxStamina;
    public float Damage;
    public float Velocity;
    public float Stamina;
    public int XP;
    public int Level;
    public Vector3 LocalScale;
    public Vector3 Position;
    public Vector3 OverWorldPosition;
    public Transform Transform;
    public int Scene;
    public int monedas;
   



    public void heal(PocionesSettings item)
    {
        switch (item.tipo)
        {
            case PocionesTipos.RevivePotion:
                if (Health <= 0)
                {
                    Health = 0;
                    Health += item.HP;
                    if (Health > HPmax)
                    {
                        Health = HPmax;
                    }
                }
                item.cantidad--;
                break;
            case PocionesTipos.Pocion:
                if (Health > 0)
                {
                    Health += item.HP;
                    if (Health > HPmax)
                    {
                        Health = HPmax;
                    }
                }
                item.cantidad--;
                break;
            case PocionesTipos.SuperPocion:
                if (Health > 0)
                {
                    Health += item.HP;
                    if (Health > HPmax)
                    {
                        Health = HPmax;
                    }
                }
                item.cantidad--;
                break;
            case PocionesTipos.expPotion:
                XP += item.Exp;
                item.cantidad--;
                break;
            case PocionesTipos.StaminaPotion:
                Stamina += item.Stamina;
                item.cantidad--;
                break;
            case PocionesTipos.Monedas:
                item.cantidad = monedas;
                break;

        }
    }
    public void equiparArmadura(ArmaduraSettings it)
    {

        HPmax += it.BonusHP;

        Velocity += it.BonusVel;

        MaxStamina += it.BonusStamina;

        Damage += it.BonusDaño;



    }
    public void desequiparArmadura(ArmaduraSettings it)
    {
        HPmax -= it.BonusHP;

        Velocity -= it.BonusVel;

        MaxStamina -= it.BonusStamina;

        Damage -= it.BonusDaño;
    }
}
