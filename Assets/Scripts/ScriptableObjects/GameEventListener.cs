﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine;
using UnityEngine.Events;

public class GameEventListener : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GameEvent Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent Response;


    public PjSettings Personaje;
    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised()
    {
        Response.Invoke();
    }

    public void OnEventRaisedItem(PocionesSettings it)
    {
        if (it.cantidad > 0)
        {
            Personaje.heal(it);
        }
    }
    public void OnEventRaisedArmor(ArmaduraSettings it)
    {
        if (it.cantidad > 0)
        {
            if (!it.Active)
            {
                it.cantidad--;
                Personaje.equiparArmadura(it);
                it.Active = true;
            }
        }
    }

    public void OnEventRaisedBuyArmor(ArmaduraSettings it){

        if (Personaje.monedas >= it.precio)
        {
            it.cantidad++;
            Personaje.monedas -= it.precio;
        }
       

    }
    public void OnEventRaisedBuyItem(PocionesSettings it)
    {

        if (Personaje.monedas >= it.precio)
        {
            it.cantidad++;
            Personaje.monedas -= it.precio;
        }
    }
    public void OnEventRaisedSellItem(PocionesSettings it)
    {
        if (it.cantidad > 0)
        {
            it.cantidad--;
            Personaje.monedas += it.precio;
        }
       
       
    }
    public void OnEventRaisedSellArmor(ArmaduraSettings it)
    {
        if (it.cantidad > 0)
        {
            it.cantidad--;
            Personaje.monedas += it.precio;
        }
    }
}
