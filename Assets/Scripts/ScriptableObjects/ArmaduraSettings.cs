﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum ArmaduraTipus
{
    Pechera,
    Botas,
    espada
    //Amuleto

}
[CreateAssetMenu(fileName = "Armadura", menuName = "ScriptableObjects/Armadura", order = 1)]
public class ArmaduraSettings : Item
{
    public int BonusHP;
    public int BonusVel;
    public int BonusDaño;
    public int BonusStamina;
    [Space]
    public ArmaduraTipus tipo;
    public bool Active;

   
}
