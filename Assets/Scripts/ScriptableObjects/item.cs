﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Scriptable object que guarda los datos de los objetos
public class Item : ScriptableObject
{
    public int cantidad;
    public Sprite imagen;
    public int nivel;
    public int precio;
}
