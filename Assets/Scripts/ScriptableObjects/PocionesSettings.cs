﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PocionesTipos
{
    Pocion,
    SuperPocion,
    RevivePotion,
    StaminaPotion,
    expPotion,
    Monedas

}
[CreateAssetMenu(fileName = "Pocion", menuName = "ScriptableObjects/Pocion", order = 1)]
public class PocionesSettings : Item
{
    public int HP;
    public int Stamina;
    public int Exp;
    [Space]
    public PocionesTipos tipo;
}



