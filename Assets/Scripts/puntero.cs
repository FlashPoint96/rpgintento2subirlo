﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puntero : MonoBehaviour
{
    int index = 0;
    private float y = 0.92f;
    private float x = 4.95f;
    /*private float y = 0.66f;
    private float x = 3.60f;
    */
    public InventoryManager iManager;
    public GameEvent evento;
    public GameEvent pjgethit;

    public ArmaduraSettings armadura;

    private void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
  
        if (Input.GetKeyDown("s"))
        {
            if (index != 5 && index != 11)
            {
                index++;
                //60 pabajo es s y -60 parriba 
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - y‬, this.transform.position.z);
            }
        }
        else if (Input.GetKeyDown("w"))
        {
            if (index != 0 && index != 6)
            {
                index--;
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + y, this.transform.position.z);
            }

        }
        else if (Input.GetKeyDown("a"))
        {
            if (index > 5)
            {
                index -= 6;
                this.transform.position = new Vector3(this.transform.position.x - x, this.transform.position.y, this.transform.position.z);
            }

        }
        else if (Input.GetKeyDown("d"))
        {
            if (index < 6)
            {
                index += 6;
                this.transform.position = new Vector3(this.transform.position.x +x, this.transform.position.y, this.transform.position.z);

            }
        }else if (Input.GetKeyDown(KeyCode.G))
        {
            if (index < 6)
            {
                evento.RaiseItem((PocionesSettings)iManager.lista[index]);
                pjgethit.Raise();
            }
            else
            {
                evento.RaiseArmadura((ArmaduraSettings)iManager.lista[index]);
             
            }
           

        }

    }

  
}
