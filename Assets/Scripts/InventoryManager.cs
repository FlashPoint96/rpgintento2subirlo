﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{

    //public GameObject cursor;
    public Text[] texto = new Text[17];
    public List<Item> lista = new List<Item>();


    public PjSettings personaje;
    //int index = 0;

    // Start is called before the first frame update
    void Start()
    {
        //texto[16] = personaje.Health.ToString();

       
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < lista.Count; i++)
        {
            if (lista[i] == null)
            {
                texto[i].text = "Empty";
            }
            else
            {
                if(lista[i].name == "Monedas")
                {

                    texto[i].text = lista[i].name + " : " + personaje.monedas;
                }

                else if (lista[i].name == "Botas" || lista[i].name == "Espada" || lista[i].name == "Armadura" || lista[i].name == "Botas+" || lista[i].name == "Espada+" || lista[i].name == "Armadura+")
                {

                    texto[i].text = lista[i].name + " LvL " + lista[i].nivel + " : " + lista[i].cantidad;

                }
                else
                {

                    texto[i].text = lista[i].name + " : " + lista[i].cantidad;
                }

            }

        }
    }

    public void deactivateArmor(ArmaduraSettings item)
    {
        if (item.Active)
        {
            item.Active = false;
            personaje.desequiparArmadura(item);
        }

    }
}
