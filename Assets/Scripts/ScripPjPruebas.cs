﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;
public class ScripPjPruebas : MonoBehaviour
{
    public Camera cam;
    private Rigidbody2D RB;
    private int velocidad = 2;

    public Tilemap Tilemap;
    // Start is called before the first frame update
    void Start()
    {
        RB = this.GetComponent<Rigidbody2D>();
        cam.gameObject.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, cam.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        Moverse();
        cam.gameObject.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, cam.transform.position.z);


    }

    private void Moverse()
    {
        if (Input.GetKey("a"))
        {

            RB.velocity = new Vector2(-velocidad, RB.velocity.y);
        }
        else if (Input.GetKey("d"))
        {
            RB.velocity = new Vector2(velocidad, RB.velocity.y);
        }
        else if (Input.GetKey("w"))
        {
            RB.velocity = new Vector2(0, velocidad);
        }
        else if (Input.GetKey("s"))
        {
            RB.velocity = new Vector2(0, velocidad * -1);
        }
        else
        {
            RB.velocity = new Vector2(0, 0);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "ForestHouse")
        {
     
            SceneManager.LoadScene(1);
            print("BocaChocho");
        }
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    Vector3 prueba = new Vector3(collision.GetContact(0).point.x, collision.GetContact(0).point.y);
    //    //Guardar todas las posiciones en un vector otromapa en un array o algo asi. Asi es da mucha pereza pero se pueda usar.
    //    Vector3 otromapa = new Vector3(13, -9, 0);
    //    collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(prueba);
    //    print(collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(prueba));
    //    if(collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(prueba)== otromapa)
    //    {
    //        print("pelutodo");
    //    }

    //}
}
