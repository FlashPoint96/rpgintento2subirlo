﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;



public class ScriptPJ : MonoBehaviour
{
    private AsyncOperation sceneAsync;

    public GameEvent[] Events;
    public Camera cam;
    private Rigidbody2D RB;
    public Animator animator;
    private String lastKey;

    public bool pegando = false;
    public bool stealth = false;
    private static bool EnOverWorld = true;
    public PjSettings settings;
    public PocionesSettings pocionesettings;

    public AudioClip clipCaminar;
    public AudioClip clipAtacar;
    public bool caminando = false;


    public float health;
    public float hpMax;
    public float damage;
    [SerializeField]
    private float velocidad;
    [SerializeField]
    private float velocidadDiag;
    public float stamina;
    public float staminaMax;
    public int xp;
    public int level;
    public int monedas;
    public Vector3 position;
    public GameObject inventario;
    public GameObject inventariocanvas;
    public GameObject tienda;
    public bool shop;
    public bool inv;
    public GameEvent LevelUp;
    private float XpPerLvl = 50;
    public GameEvent UpdateStamina;
    //Invoca al evento de LevelUp Y PJGetHit pa actualizar las UIS.
    // Start is called before the first frame update
    void Start()
    {
        LevelUp.Raise();
        foreach (GameEvent Ge in Events)
        {
            if (Ge.name.Equals("PJGetHit"))
            {
                Ge.Raise();
            }
        }
        inv = false;
        velocidad = settings.Velocity;
        cam = FindObjectOfType<Camera>();
        RB = this.GetComponent<Rigidbody2D>();
        cam.gameObject.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, cam.transform.position.z);
    }
    //Se mueve y pega. Y carga datos mira su experiencia mira si esta usando Stealth y si esta en movimiento.
    // Update is called once per frame
    void Update()
    {
        if (!cam)
        {
            cam = FindObjectOfType<Camera>();
        }
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            if (Input.GetKeyDown(KeyCode.T) && shop == false)
            {
                tienda.SetActive(true);
                shop = true;
            }
            else if (Input.GetKeyDown(KeyCode.T) && shop)
            {
                tienda.SetActive(false);
                shop = false;
            }

        }
 
        if (Input.GetMouseButtonDown(0) && this.settings.Stamina>=10)
        {
            StartCoroutine(Pegar()); 
            settings.Stamina -= 10;
            UpdateStamina.Raise();
        }
        if (Input.GetKey(KeyCode.F))
        {
            this.settings.XP++;
            this.settings.Health--;
        }
        if (Input.GetKeyDown(KeyCode.I) && inv==false)
        {
            inventariocanvas.SetActive(true);
            inv = true;
           

        }
        else if (Input.GetKeyDown(KeyCode.I) && inv)
        {
            inventariocanvas.SetActive(false);
            inv = false;
        }
        cam.gameObject.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, cam.transform.position.z);
        CargarDatos();
        CheckXP();
        Stealth();
        Movement();
        Restore();
    }
    //Si esta en escena numero 5 se regenera la vida y la stamina.
    private void Restore()
    {
        if (SceneManager.GetActiveScene().buildIndex == 5)
        {
            if (settings.Health < settings.HPmax)
            {
                settings.Health += 0.5f;
                foreach (GameEvent Ge in Events)
                {
                    if (Ge.name.Equals("PJGetHit"))
                    {
                        Ge.Raise();
                    }
                }
            }
            if (settings.Stamina < settings.MaxStamina)
            {
                settings.Stamina += 0.1f;
                UpdateStamina.Raise();
            }
        }
    }
    //Mira la experiencia.
    private void CheckXP()
    {
        if (settings.XP >= XpPerLvl)
        {
            this.settings.Level++;
            LevelUp.Raise();
            settings.XP = 0;
            XpPerLvl += 50*1.5f;
            LevelUpStats();
            foreach (GameEvent Ge in Events)
            {
                if (Ge.name.Equals("PJGetHit"))
                {
                    Ge.Raise();
                }
            }
        }
    }
    //Sube las stats al subir de nivel.
    private void LevelUpStats()
    {
        this.settings.Damage = this.settings.Damage + 5;
        this.settings.HPmax = this.settings.HPmax + 20;
        //this.settings.Health = this.settings.HPmax;
        //this.settings.Stamina = this.settings.MaxStamina;
        UpdateStamina.Raise();
    }
    //Guardar el objeto.
    public void SaveScrObject()
    {
        settings.Health = this.health;
        settings.HPmax = this.hpMax;
        settings.Damage = this.damage;
        settings.Velocity = this.velocidad;
        settings.Stamina = this.stamina;
        settings.XP = this.xp;
        settings.Level = this.level;
        settings.Position = this.transform.position;
        if (SceneManager.GetActiveScene().buildIndex == SceneManager.GetActiveScene().buildIndex)
        {
            settings.Scene = SceneManager.GetActiveScene().buildIndex;
        }
        {
            settings.Scene = SceneManager.GetActiveScene().buildIndex;
        }
    }
    //Cuando le dan una ostia.
    internal void GetDamaged(float damage)
    {
        this.settings.Health -= damage;
        foreach (GameEvent Ge in Events)
        {
            if (Ge.name.Equals("PJGetHit"))
            {
                Ge.Raise();
            }
        }

    }
    //Cargar este objeto.
    public void LoadScrObject()
    {
        this.health = settings.Health;
        this.hpMax = settings.HPmax;
        this.damage = settings.Damage;
        this.velocidad = settings.Velocity;
        this.stamina = settings.Stamina;
        this.xp = settings.XP;
        this.level = settings.Level;
        this.transform.position = settings.Position;
        if (settings.Scene == SceneManager.GetActiveScene().buildIndex)
        {

        }
        else
        {
            StartCoroutine(SceneSwitch(settings.Scene, SceneManager.GetActiveScene().buildIndex, this.transform.position));

        }
    }
    //Cargar datos del persona y que esten siempre igualados.
    public void CargarDatos()
    {
        this.health = settings.Health;
        this.hpMax = settings.HPmax;
        this.damage = settings.Damage;
        this.velocidad = settings.Velocity;
        this.stamina = settings.Stamina;
        this.xp = settings.XP;
        this.level = settings.Level;
        this.monedas = settings.monedas;
    }
    //Va mas lento pero los enemigos le ven menos.
    private void Stealth()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            velocidad /= 2;
            velocidadDiag  = velocidad * 0.75f;
            stealth = true;
        }
        else if (Input.GetKey(KeyCode.Space) && settings.Stamina > 1)
        {
            velocidad *= 2;
            velocidadDiag = velocidad * 0.75f;
            settings.Stamina -= 0.1f;
            UpdateStamina.Raise();
        }
        else
        {
            velocidad = settings.Velocity;
            velocidadDiag = velocidad * 0.75f;
            stealth = false;
        }
    }

    //Moverse
    private void Movement()
    {
        if (settings.Stamina < 100)
        {
            settings.Stamina += 0.005f;
            UpdateStamina.Raise();
        }
        if (!pegando && !inv)
        {
            if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.S))
            {
                if (!caminando) this.GetComponent<AudioSource>().Play();
                caminando = true;
                RB.velocity = new Vector2(-velocidadDiag, -velocidadDiag);
                lastKey = "a";
                animator.SetTrigger("WalkA");
            }
            else if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.S))
            {
                if (!caminando) this.GetComponent<AudioSource>().Play();
                caminando = true;
                lastKey = "d";
                animator.SetTrigger("WalkD");
                RB.velocity = new Vector2(velocidadDiag, -velocidadDiag);
            }
            else if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.W))
            {
                if (!caminando) this.GetComponent<AudioSource>().Play();
                caminando = true;
                lastKey = "a";
                animator.SetTrigger("WalkA");
                RB.velocity = new Vector2(-velocidadDiag, velocidadDiag);
            }
            else if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.W))
            {
                if (!caminando) this.GetComponent<AudioSource>().Play();
                caminando = true;
                lastKey = "d";
                animator.SetTrigger("WalkD");
                RB.velocity = new Vector2(velocidadDiag, velocidadDiag);
            }
            else if (Input.GetKey(KeyCode.W))
            {
                if (!caminando) this.GetComponent<AudioSource>().Play();
                caminando = true;
                lastKey = "w";
                animator.SetTrigger("WalkW");
                RB.velocity = new Vector2(0, velocidad);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                if (!caminando) this.GetComponent<AudioSource>().Play();
                caminando = true;
                lastKey = "s";
                animator.SetTrigger("WalkS");
                RB.velocity = new Vector2(0, velocidad * -1);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                if (!caminando) this.GetComponent<AudioSource>().Play();
                caminando = true;
                lastKey = "d";
                animator.SetTrigger("WalkD");
                RB.velocity = new Vector2(velocidad, 0);
            }
            else if (Input.GetKey(KeyCode.A))
            {
                if (!caminando) this.GetComponent<AudioSource>().Play();
                caminando = true;
                lastKey = "a";
                animator.SetTrigger("WalkA");
                RB.velocity = new Vector2(-velocidad, 0);
            }
            else
            {
                caminando = false;
                this.GetComponent<AudioSource>().Stop();
                RB.velocity = new Vector2(0, 0);
                //Si no te mueves, regeneras energia
                if (settings.Stamina < 100)
                {
                    settings.Stamina += 0.1f;
                    UpdateStamina.Raise();
                }
            }
        }
        else
        {
            RB.velocity = new Vector2(0, 0);
        }
    }


    //Pegar.
    IEnumerator Pegar()
    {
        if (!inv)
        {
            AudioSource.PlayClipAtPoint(clipAtacar, this.transform.position);
            pegando = true;
            if (lastKey == "a")
            {
                animator.SetTrigger("AttackA");
            }
            else if (lastKey == "d")
            {
                animator.SetTrigger("AttackD");
            }
            else if (lastKey == "s")
            {
                animator.SetTrigger("AttackS");

            }
            else if (lastKey == "w")
            {
                animator.SetTrigger("AttackW");
            }
            yield return new WaitForSeconds(1f);
            pegando = false;
        }
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
       
            

        if (collision.gameObject.tag.Equals("Hermitaño"))
        {
            //print("Abrir tienda");
        }
    }
    //Cuando entra en colision con alguna cosa y si es alguna puerta o algo.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.tag.Equals("Enemy"))
        {           
            if (this.settings.Health <= 0)
            {
                SceneManager.LoadScene(6);
            }
            foreach(GameEvent Ge in Events)
            {
                    if (Ge.name.Equals("PJGetHit"))
                    {
                    }
                    Ge.Raise();            }
        }

        else
        {
            Vector3 pjPosition = new Vector3(collision.GetContact(0).point.x, collision.GetContact(0).point.y);
            collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition);
            Vector3[] ForrestEnter = { new Vector3(13, -9, 0), new Vector3(13, -10, 0), new Vector3(12, -9, 0), new Vector3(12, -10, 0)};
            Vector3[] Forrestexit = { new Vector3(0, -5, 0), new Vector3(0, -4, 0), new Vector3(-1, -4), new Vector3(-1, -5) };
            Vector3[] MontainEnter = { new Vector3(-59, 52, 0), new Vector3(-59, 53, 0)};
            Vector3[] MontainExit= { new Vector3(0, -5, 0), new Vector3(0,-4, 0) };
            Vector3[] CaveEnter = { new Vector3(-18, 52, 0), new Vector3(-18, 53, 0), new Vector3(-17, 52, 0), new Vector3(-17, 53, 0) };
            Vector3[] CaveExit = { new Vector3(1, -4, 0), new Vector3(1, -3, 0), new Vector3(1, -5, 0)};
            Vector3[] RestoreEnter = { new Vector3(-3, 62, 0), new Vector3(-3, 63, 0), new Vector3(-73, -48, 0), new Vector3(-2, -45, 0), new Vector3(-3, -2, 0) };
            Vector3[] RestoreExit = { new Vector3(-11, 0, 0), new Vector3(-11, -2, 0), new Vector3(-8, 3, 0)};
            print(collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition));


            foreach (Vector3 enter in RestoreEnter)
            {
                if (enter == collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition) && SceneManager.GetActiveScene().buildIndex == 0)
                {
                    this.settings.OverWorldPosition = this.transform.position;
                    StartCoroutine(SceneSwitch(5, 0, new Vector2(0, 0)));
                }
            }
            foreach (Vector3 exit in RestoreExit)
            {
                if (exit == collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition) && SceneManager.GetActiveScene().buildIndex == 5)
                {
                    StartCoroutine(SceneSwitchVuelta(0, 5));

                }
            }
            foreach (Vector3 enter in ForrestEnter)
            {
                if(enter== collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition) && SceneManager.GetActiveScene().buildIndex == 0)
                {
                    this.settings.OverWorldPosition = this.transform.position;
                    StartCoroutine(SceneSwitch(1, 0, new Vector2(0, 0)));
                }
            }
            foreach (Vector3 exit in Forrestexit)
            {
                if (exit == collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition) && SceneManager.GetActiveScene().buildIndex == 1)
                {
                    StartCoroutine(SceneSwitchVuelta(0, 1));

                }
            }
            foreach(Vector3 enter in MontainEnter)
            {
                if(enter== collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition) && SceneManager.GetActiveScene().buildIndex == 0)
                {
                    this.settings.OverWorldPosition = this.transform.position;
                    StartCoroutine(SceneSwitch(2, 0, new Vector2(0, 0)));
                }
            }
            foreach (Vector3 exit in MontainExit)
            {
                if (exit == collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition) && SceneManager.GetActiveScene().buildIndex == 2)
                {
                    StartCoroutine(SceneSwitchVuelta(0, 2));
                }
            }
            foreach(Vector3 enter in CaveEnter)
            {
                if (enter == collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition) && SceneManager.GetActiveScene().buildIndex ==0)
                {
                    this.settings.OverWorldPosition = this.transform.position;
                    StartCoroutine(SceneSwitch(3, 0, new Vector2(2, 0)));
                }
            }
            foreach (Vector3 exit in CaveExit)
            {
                if (exit == collision.gameObject.GetComponent<Tilemap>().layoutGrid.WorldToCell(pjPosition) && SceneManager.GetActiveScene().buildIndex == 3)
                {
                    StartCoroutine(SceneSwitchVuelta(0, 3));
                }
            }
        }
    }

    //Cargar OverWorld y descargar la escena en la que estava.
    IEnumerator SceneSwitchVuelta(int v1, int v2)
    {
        AsyncOperation load = SceneManager.LoadSceneAsync(v1, LoadSceneMode.Additive);
        yield return load;
        SceneManager.MoveGameObjectToScene(this.gameObject, SceneManager.GetSceneByBuildIndex(v1));
        this.transform.position = this.settings.OverWorldPosition;
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(v1));
        SceneManager.UnloadSceneAsync(v2);
    }
    //Descargar OverWorld y cargar la nueva escena.
    IEnumerator SceneSwitch(int v, int v1,Vector2 pos)
    {
        AsyncOperation load = SceneManager.LoadSceneAsync(v, LoadSceneMode.Additive);
        yield return load;
        SceneManager.MoveGameObjectToScene(this.gameObject, SceneManager.GetSceneByBuildIndex(v));
        this.transform.position = pos;
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(v));
        SceneManager.UnloadSceneAsync(v1);
    }


  
}
