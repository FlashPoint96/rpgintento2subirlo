﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class EnemyLvLUI : MonoBehaviour
{
    private TextMeshProUGUI texto;

    // Start is called before the first frame update
    void Start()
    {
        texto = GetComponent<TextMeshProUGUI>();
        texto.text = "LvL: " + this.GetComponentInParent<EnemyScript>().ES.Level.ToString();
    }

}
