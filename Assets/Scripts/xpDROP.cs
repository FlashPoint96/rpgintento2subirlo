﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class xpDROP : MonoBehaviour
{
    public int XP;
    public Sprite Botas;
    public Sprite Armadura;
    public Sprite Katuna;
    private SpriteRenderer spriteRenderer;
    private int a;
    public GameObject inventory;
    //Un random del 1 al 10 para ver si dropea algun item o no.
    private void Start()
    {
        spriteRenderer = this.GetComponent<SpriteRenderer>();
        inventory = GameObject.Find("Inventario");
        a = Random.RandomRange(0, 10);
        if (a == 1)
        {
            spriteRenderer.sprite = Armadura;
            this.transform.localScale = new Vector3(0.3f,0.3f,0.3f);
        }
        else if (a == 2)
        {
            spriteRenderer.sprite = Botas;
            this.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
        }
        else if (a == 3)
        {
            spriteRenderer.sprite = Katuna;
            this.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        }
    }

    //Al colisionar con el PJ le da XP y monedas random que lsa puede usar en la tienda.Y si le ha tocado un item pues se lo suma en el inventario.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        inventory = GameObject.Find("Inventario");
        if (collision.gameObject.name == "HurtBox")
        {
            collision.gameObject.GetComponentInParent<ScriptPJ>().settings.XP += XP;
            collision.gameObject.GetComponentInParent<ScriptPJ>().settings.monedas += Random.RandomRange(0,5);
            inventory.GetComponentInChildren<InventoryManager>().lista[5].cantidad = collision.gameObject.GetComponentInParent<ScriptPJ>().settings.monedas;
             if (a == 1)
             {
                if (inventory.GetComponentInChildren<InventoryManager>().lista[6].cantidad == 0){
                inventory.GetComponentInChildren<InventoryManager>().lista[6].cantidad++;
                }
            }else if (a == 2)
            {
                if (inventory.GetComponentInChildren<InventoryManager>().lista[7].cantidad == 0)
                {
                    inventory.GetComponentInChildren<InventoryManager>().lista[7].cantidad++;
                }
            }
            else if (a == 3)
            {
                if (inventory.GetComponentInChildren<InventoryManager>().lista[8].cantidad == 0)
                {
                    inventory.GetComponentInChildren<InventoryManager>().lista[8].cantidad++;
                }
            }
            GameObject.Destroy(this.gameObject);
        }
        //GameObject.Destroy(this.gameObject);
    }

}
