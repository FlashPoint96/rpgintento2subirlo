﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    //contador de enemigos activos
    int cont;
    public float segundos;
    // Start is called before the first frame update
    void Start()
    {
        cont = 0;
        Invoke("InvocarEnemysEmpezar", 0.0f);
        StartCoroutine(EnemySpawn());
        //InvokeRepeating("InvocarEnemys", 0f, 15f);
    }
    //Invoca unos enemigos al empezar.
    void InvocarEnemysEmpezar()
    {
        for (int i = 0; i < 5; i++)
        {
            GameObject Enemy = PoolEnemys.Instance.GetEnemys();
            if (Enemy != null)
            {
                Enemy.SetActive(true);
                Enemy.gameObject.GetComponent<EnemyScript>().CargarCosasNoRandom(3);
                Enemy.gameObject.GetComponent<EnemyScript>().GetHit.Raise();
            }
        }
        for (int i = 0; i < 3; i++)
        {
            GameObject Enemy = PoolEnemys.Instance.GetEnemys();
            if (Enemy != null)
            {
                Enemy.SetActive(true);
                Enemy.gameObject.GetComponent<EnemyScript>().CargarCosasNoRandom(6);
                Enemy.gameObject.GetComponent<EnemyScript>().GetHit.Raise();
            }
        }
        GameObject Enemy2 = PoolEnemys.Instance.GetEnemys();
        if (Enemy2 != null)
        {
            Enemy2.SetActive(true);
            Enemy2.gameObject.GetComponent<EnemyScript>().CargarCosasNoRandom(7);
            Enemy2.gameObject.GetComponent<EnemyScript>().GetHit.Raise();
        }
    }
    //Una corrutina pa invocar enemigos.
    IEnumerator EnemySpawn()
    {
        yield return new WaitForSeconds(2);
        while (true)
        {
            GameObject Enemy = PoolEnemys.Instance.GetEnemys();
            if (Enemy != null)
            {
                //Enemy.transform.position = new Vector2(0, 0);
                Enemy.SetActive(true);
               Enemy.gameObject.GetComponent<EnemyScript>().CargarCosas();
              Enemy.gameObject.GetComponent<EnemyScript>().GetHit.Raise();
                
                //cont++;
            }
            yield return new WaitForSeconds(segundos);
        }
    }
}
