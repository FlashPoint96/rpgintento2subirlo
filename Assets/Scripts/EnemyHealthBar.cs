﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthBar : MonoBehaviour
{
    private float MaxVida;
    private float VidaActual;
    public void ActualizarVida()
    {
        MaxVida = this.GetComponentInParent<EnemyScript>().ES.MaxHealth;
        VidaActual = this.GetComponentInParent<EnemyScript>().Health;
        this.GetComponent<Image>().fillAmount = VidaActual / MaxVida;
    }
}
