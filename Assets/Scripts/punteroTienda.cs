﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class punteroTienda : MonoBehaviour
{
    int index = 0;
    private float y = 0.945f;
    private float x = 6.25f;
    public InventoryManager iManager;
    public GameEvent evento;
    public ArmaduraSettings armadura;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

      

        if (Input.GetKeyDown("s"))
        {
            if (index != 4 && index != 10)
            {
                index++;
                //60 pabajo es s y -60 parriba 
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - y‬, this.transform.position.z);
            }
        }
        else if (Input.GetKeyDown("w"))
        {
            if (index != 0 && index != 6)
            {
                index--;
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + y, this.transform.position.z);
            }

        }
        else if (Input.GetKeyDown("a"))
        {
            if (index > 5)
            {
                index -= 6;
                this.transform.position = new Vector3(this.transform.position.x - x, this.transform.position.y, this.transform.position.z);
            }

        }
        else if (Input.GetKeyDown("d"))
        {
            if (index < 6)
            {
                index += 6;
                this.transform.position = new Vector3(this.transform.position.x + x, this.transform.position.y, this.transform.position.z);

            }
        }
        else if (Input.GetKeyDown(KeyCode.G))
        {
            if (index > 4)
            {
                //comprar armadura
                evento.BuyArmor((ArmaduraSettings)iManager.lista[index]);
      
            }
            else if(index<=4)
            {
                //comprar pociones 
                evento.BuyItem((PocionesSettings)iManager.lista[index]);

            }
        }else if (Input.GetKeyDown(KeyCode.H))
        {
            if (index > 4)
            {
                //vender armadura
                evento.SellArmor((ArmaduraSettings)iManager.lista[index]);

            }
            else if (index <= 4)
            {
                //vender potis
                evento.SellItem((PocionesSettings)iManager.lista[index]);
            }


        }
       
    }

}
