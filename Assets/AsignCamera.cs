﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsignCamera : MonoBehaviour
{
    public Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        this.cam  = FindObjectOfType<Camera>();
        this.GetComponent<Canvas>().worldCamera = this.cam;
    }

    // Update is called once per frame
    void Update()
    {
        if (!cam)
        {
            this.cam = FindObjectOfType<Camera>();
            this.GetComponent<Canvas>().worldCamera = this.cam;
        }
    }
}
